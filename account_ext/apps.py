from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AccountExtConfig(AppConfig):
    name = 'account_ext'
    verbose_name = _('Accounts')
