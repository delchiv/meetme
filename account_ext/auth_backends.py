from allauth.account import auth_backends


class AuthenticationBackend(auth_backends.AuthenticationBackend):
    def _authenticate_by_username(self, **credentials):
        username = credentials.get('username')
        if username.find('@') == -1:
            username = username.replace("+", "").replace("(", "").replace(")", "").replace("+", "").replace("-", "").replace(" ", "").strip()
        credentials["username"] = username
        return super(AuthenticationBackend, self)._authenticate_by_username(**credentials)