from django.conf.urls import url
from django.urls import reverse_lazy

from . import views


redirect_to_profile = views.RedirectView.as_view(url=reverse_lazy('account_profile'), permanent=False)

urlpatterns = [
    url(r"^login/$", views.login, name="account_login"),
    url(r"^signup/$", views.signup, name="account_signup"),
    url(r"^signup-public/$", views.signup_public, name="account_signup_public"),
    url(r"^accounts/profile/$", views.profile,
        name="account_profile"),
    url(r"^password/reset/done/$", views.password_reset_done,
        name="account_reset_password_done"),
    url(r"^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$",
        views.password_reset_from_key,
        name="account_reset_password_from_key"),
    url(r"^password/reset/key/done/$", views.password_reset_from_key_done,
        name="account_reset_password_from_key_done"),
    url(r"^accounts/profile/meetings/$", views.MeetingsListView.as_view(),
        name="account_profile_meetings"),

    url(r"^inactive/$", views.account_inactive, name="account_inactive"),
    url(r'^social/connections/$', redirect_to_profile, name='socialaccount_connections'),
    url(r'^login/cancelled/$', redirect_to_profile, name='socialaccount_login_cancelled'),
    url(r'^login/error/$', redirect_to_profile, name='socialaccount_login_error'),

    url(r'^profile/(?P<pk>\d+)/$', views.UserDetailView.as_view(), name='person-detail'),

    url(r'^approve/(?P<pk>\d+)/$', views.ApproveView.as_view(), name='person-approve'),
]
