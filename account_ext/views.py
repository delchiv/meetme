from django.http import JsonResponse, Http404
from django.shortcuts import redirect, reverse
from django.views.generic import TemplateView, FormView, UpdateView, RedirectView, DetailView, DetailView, ListView
from django.contrib.auth import get_user_model
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import pgettext, ugettext as _
from django.contrib.admin.views.decorators import staff_member_required

from allauth.account import views
from allauth.account.utils import get_request_param, get_next_redirect_url
from allauth.exceptions import ImmediateHttpResponse

from comments.models import Comment
from meetings.forms import MeetingCreateForm
from meetings.models import Meeting

from .forms import LoginForm, SignupForm, SignupPublicForm, SMSForm, UserProfileForm, ApproveForm
from .models import SMSConfirmation

# Create your views here.

class AjaxOnlyResponseMixin(object):

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            html = super(AjaxOnlyResponseMixin, self).get(request, *args, **kwargs)
            return JsonResponse({"html": html.rendered_content})
        raise Http404


class LoginView(views.LoginView):
    form_class = LoginForm

    def dispatch(self, request, *args, **kwargs):
        try: del request.session["last_inactive_user"]
        except: pass
        if request.method == "GET" and not request.is_ajax():
            if request.user.is_authenticated:
                return redirect("account_profile")
            request.session['ajax_after_done'] = request.get_full_path()
            return redirect("index:index")
        return super(LoginView, self).dispatch(request, *args, **kwargs)

login = LoginView.as_view()


class SignupView(views.SignupView):
    form_class = SignupForm
    redirect_field_name = "next"

    def dispatch(self, request, *args, **kwargs):
        try: del request.session["last_inactive_user"]
        except: pass
        if request.method == "GET" and not request.is_ajax():
            if request.user.is_authenticated:
                return redirect("account_profile")
            request.session['ajax_after_done'] = request.get_full_path()
            return redirect("index:index")
        return super(SignupView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SignupView, self).get_context_data(**kwargs)

        redirect_field_name = self.redirect_field_name
        redirect_field_value = get_request_param(self.request,
                                                 redirect_field_name)
        context["form_action"] = reverse('account_signup')

        if redirect_field_value:
            context["redirect_field_value"] = redirect_field_value
        else:
            try: del context["redirect_field_value"]
            except: pass

        return context


signup = SignupView.as_view()


class SignupPublicView(SignupView):
    form_class = SignupPublicForm

    def get_context_data(self, **kwargs):
        context = super(SignupPublicView, self).get_context_data(**kwargs)
        context["form_action"] = reverse('account_signup_public')
        return context


signup_public = SignupPublicView.as_view()


class PasswordResetDoneView(AjaxOnlyResponseMixin, views.PasswordResetDoneView):
    pass

password_reset_done = PasswordResetDoneView.as_view()


class PasswordResetFromKeyView(views.PasswordResetFromKeyView):

    def dispatch(self, request, uidb36, key, **kwargs):
        if request.method == "GET" and not request.is_ajax():
            request.session['ajax_after_done'] = request.get_full_path()
            return redirect("index:index")
        return super(PasswordResetFromKeyView, self).dispatch(request,
                                                                  uidb36,
                                                                  key,
                                                                  **kwargs)

password_reset_from_key = PasswordResetFromKeyView.as_view()


class PasswordResetFromKeyDoneView(AjaxOnlyResponseMixin, views.PasswordResetFromKeyDoneView):
    pass

password_reset_from_key_done = PasswordResetFromKeyDoneView.as_view()


@method_decorator(login_required, name='dispatch')
class ProfileView(SuccessMessageMixin, UpdateView):
    template_name = "account/profile.html"
    form_class = UserProfileForm
    model = get_user_model()
    success_message = _('Данные успешно сохранены.')

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse("account_profile")

profile = ProfileView.as_view()


class AccountInactiveView(views.AjaxCapableProcessFormViewMixin, FormView):
    form_class = SMSForm
    template_name = 'account/account_inactive.html'
    redirect_field_name = "next"

    def dispatch(self, request, *args, **kwargs):
        if not self.request.session.has_key("last_inactive_user"):
            raise Http404
        if request.method == "GET" and not request.is_ajax() and request.session.has_key("last_inactive_user"):
            request.session['ajax_after_done'] = request.get_full_path()
            return redirect("index:index")
        elif request.POST.get("action") == "send-sms":
            return self.send_sms(request)
        return super(AccountInactiveView, self).dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super(AccountInactiveView, self).get_initial()

        try: initial['username'] = self.request.session.get("last_inactive_user")
        except: pass

        return initial

    def send_sms(self, request):
        try: user = get_user_model().objects.get(username=self.request.session.get("last_inactive_user"))
        except: user = None
        if user:
            try:
                sms = SMSConfirmation.objects.filter(user=user)[0]
                if sms.key_expired():
                    sms.send(request)
            except:
                sms = SMSConfirmation.create(user)
                sms.send(request)
        url = reverse("account_inactive")
        next = self.get_success_url()
        if next:
            url+="?%s=%s" % (self.redirect_field_name, next)
        return redirect(url)

    def form_valid(self, form):
        user = get_user_model().objects.get(username=form.cleaned_data['username'])
        user.is_active = True
        user.save()
        try: del self.request.session["last_inactive_user"]
        except: pass
        success_url = self.get_success_url()
        try:
            return form.login(self.request, user, redirect_url=success_url)
        except ImmediateHttpResponse as e:
            return e.response

    def get_success_url(self):
        ret = (get_next_redirect_url(
            self.request,
            self.redirect_field_name) or self.success_url)
        return ret

    def get_context_data(self, **kwargs):
        context = super(AccountInactiveView, self).get_context_data(**kwargs)
        redirect_field_value = get_request_param(self.request,
                                                 self.redirect_field_name)
        if redirect_field_value:
            context["redirect_field_value"] = redirect_field_value
        context["redirect_field_name"] = self.redirect_field_name
        try:
            username = self.request.session.get("last_inactive_user")
            context["sms_expired"] = SMSConfirmation.objects.filter(user__username=username).order_by("-created")[0].key_expired_at()
        except:
            pass

        return context

account_inactive = AccountInactiveView.as_view()


@method_decorator(login_required, name='dispatch')
class UserDetailView(DetailView):
    model = get_user_model()

    def dispatch(self, request, *args, **kwargs):
        object = self.get_object()
        if request.user and not request.user.is_superuser:
            if object.is_public and not object.is_approved:
                raise Http404
            elif not object.is_public:
                raise Http404
        return super(UserDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)

        context["comments"] = Comment.objects.filter(cto=self.object).order_by('-created')
        meeting = Meeting.objects.filter(mfrom=self.request.user, mto=self.object).order_by("-changed")
        if meeting:
            context["meeting"] = meeting[0] # Последняя измененная встреча
        elif self.object.is_public and self.object.is_approved:
            context["form"] = MeetingCreateForm(initial={"mfrom":self.request.user, "mto":self.object})

        return context


@method_decorator(login_required, name='dispatch')
@method_decorator(staff_member_required, name='dispatch')
class ApproveView(AjaxOnlyResponseMixin, UpdateView):
    model = get_user_model()
    form_class = ApproveForm
    template_name = "account_ext/user_approve.html"

    def form_valid(self, form):
        self.object.is_approved = True
        self.object.save()
        return JsonResponse({"location": reverse('person-detail', kwargs={"pk": self.object.pk})})


@method_decorator(login_required, name='dispatch')
class MeetingsListView(ListView):
    model = Meeting

    def get_queryset(self):
        return Meeting.objects.filter(mfrom=self.request.user)