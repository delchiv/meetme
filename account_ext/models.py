# coding: utf-8

import random
import datetime

from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.mail import send_mail
from django.utils.translation import pgettext, ugettext as _
from django.utils import timezone
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.shortcuts import reverse
from django.contrib.sites.shortcuts import get_current_site

from allauth.account.adapter import get_adapter
from index.models import Category

from .managers import SMSConfirmationManager

# Create your models here.

class User(AbstractUser):
    photo = models.ImageField(_('Фото'), upload_to='users/%Y/%m/%d', blank=True, null=True)
    nickname = models.CharField(_('Ник'), max_length=250, blank=True, null=True)
    is_public = models.BooleanField(_('Публичный?'), default=False)
    about = models.TextField(_('О себе'), blank=True, null=True)
    categories = models.ManyToManyField(Category, verbose_name=_('Категории'), blank=True)
    rate = models.FloatField(_('Оценка'), default=0.0)
    is_approved = models.BooleanField(_('Подтвержден?'), default=False)

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.__unicode__()

    def __unicode__(self):
        res = self.first_name
        res += " " + self.last_name
        res = res.strip()
        if not len(res):
            res = self.username
        return res

    def recalc_rate(self):
        from comments.models import Comment
        self.rate =  Comment.objects.filter(cto=self).aggregate(models.Avg('rate'))['rate__avg']
        self.save()


class SMSConfirmation(models.Model):
    user = models.ForeignKey(User,
                                      verbose_name=_('User'),
                                      on_delete=models.CASCADE)
    created = models.DateTimeField(verbose_name=_('created'),
                                   default=timezone.now)
    sent = models.DateTimeField(verbose_name=_('sent'), null=True)
    key = models.IntegerField(verbose_name=_('key'))

    objects = SMSConfirmationManager()

    class Meta:
        verbose_name = _("SMS подтверждение")
        verbose_name_plural = _("SMS подтверждения")

    def __unicode__(self):
        return _("SMS подтверждение для %s") % self.user

    @classmethod
    def create(cls, user):
        key = random.randint(0000, 9999)
        return cls._default_manager.create(user=user,
                                           key=key)

    def key_expired(self):
        try: seconds = settings.SMS_CONFIRMATION_EXPIRE_SECONDS
        except: seconds = 3*60

        expiration_date = self.sent \
            + datetime.timedelta(seconds=seconds)
        return expiration_date <= timezone.now()
    key_expired.boolean = True

    def key_expired_at(self):
        try: seconds = settings.SMS_CONFIRMATION_EXPIRE_SECONDS
        except: seconds = 3*60

        delta = (timezone.now() - self.sent).seconds
        if delta < seconds:
            return seconds - delta
        return 0

    def confirm(self, request):
        if not self.key_expired() and not self.user.is_active:
            user = self.user
            get_adapter(request).confirm_sms(request, user)
            #signals.email_confirmed.send(sender=self.__class__,
            #                             request=request,
            #                             email_address=email_address)
            return user

    def send(self, request=None, signup=False):
        get_adapter(request).send_confirmation_sms(request, self, signup)
        #signals.email_confirmation_sent.send(sender=self.__class__,
        #                                     request=request,
        #                                     confirmation=self,
        #                                     signup=signup)


@receiver(pre_save, sender=User)
def notify_approvement(sender, **kwargs):
    instance = kwargs.pop("instance")
    if instance.id:
        old_is_approved = instance.__class__.objects.get(id=instance.id).is_approved
        if instance.is_approved != old_is_approved and instance.is_approved and instance.email:
            current_site = get_current_site(request=None)
            text = 'Ваша учетная запись подтверждена. <a href="http://%s%s">%s</a>' % (current_site.domain, reverse('person-detail', kwargs={"pk":instance.id}), str(instance))
            send_mail(
                'Изменение статуса',
                text,
                'no-reply@meetmehub.com',
                [instance.email],
                fail_silently=False,
                html_message=text,
            )
