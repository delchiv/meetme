from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import pgettext, ugettext as _

from .models import SMSConfirmation, User


class SMSConfirmationAdmin(admin.ModelAdmin):
    list_display = ('user', 'created', 'sent', 'key')
    list_filter = ('sent',)
    raw_id_fields = ('user',)


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('photo', 'username', 'password')}),
        (_('Personal info'), {'fields': ('nickname', 'first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Public person'), {'fields': ('is_approved', 'is_public', 'about', 'categories')}),
    )

admin.site.register(User, CustomUserAdmin)
admin.site.register(SMSConfirmation, SMSConfirmationAdmin)

