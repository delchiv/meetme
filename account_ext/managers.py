from datetime import timedelta

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone


class SMSConfirmationManager(models.Manager):

    def all_expired(self):
        return self.filter(self.expired_q())

    def all_valid(self):
        return self.exclude(self.expired_q())

    def expired_q(self):
        try: seconds = settings.SMS_CONFIRMATION_EXPIRE_SECONDS
        except: seconds = 3*60
        sent_threshold = timezone.now() \
            - timedelta(seconds=seconds)
        return Q(sent__lt=sent_threshold)

    def delete_expired_confirmations(self):
        self.all_expired().delete()
