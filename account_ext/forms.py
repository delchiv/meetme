from django import forms
from django.contrib.auth import get_user_model
from django.shortcuts import reverse
from django.core.mail import send_mail
from django.utils.translation import pgettext, ugettext as _
from django.conf import settings

from allauth.account.forms import LoginForm as LoginFormOld, SignupForm as SignupFormOld
from allauth.utils import set_form_field_order
from allauth.account.utils import perform_login
from allauth.account import app_settings
from allauth.account.adapter import get_adapter

from index.models import Category
from .models import SMSConfirmation


class LoginForm(LoginFormOld):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(LoginForm, self).__init__(*args, **kwargs)
        login_widget = forms.TextInput(attrs={'placeholder':
                                              _('Телефон или e-mail'),
                                              'autofocus': 'autofocus'})
        login_field = forms.CharField(label=pgettext("field label",
                                                     "Login"),
                                      widget=login_widget)
        self.fields["login"] = login_field
        set_form_field_order(self, ["login", "password", "remember"])
        if app_settings.SESSION_REMEMBER is not None:
            del self.fields['remember']

    def login(self, request, redirect_url=None):
        if not self.user.is_active:
            request.session["last_inactive_user"] = self.user.username
            adapter = get_adapter(request)
            return adapter.respond_user_inactive(request, self.user, redirect_url)

        return super(LoginForm, self).login(request, redirect_url)


class SignupForm(SignupFormOld):
    username = forms.CharField(label=_("Телефон"),
                               min_length=app_settings.USERNAME_MIN_LENGTH,
                               widget=forms.TextInput(
                                   attrs={'placeholder': _('Телефон'),
                                          'autofocus': 'autofocus',
                                          'data-inputmask': "'alias': 'phone'",
                                          }))

    def save(self, request):
        user = super(SignupForm, self).save(request)
        user.is_active = False
        user.save()
        request.session["last_inactive_user"] = user.username
        sms = SMSConfirmation.create(user)
        sms.send(request, True)
        return user


class SignupPublicForm(SignupForm):
    categories = forms.ModelMultipleChoiceField(label=_("Категории"), queryset=Category.objects.all(), required=True)

    def save(self, request):
        user = super(SignupPublicForm, self).save(request)
        user.is_public = True
        user.is_approved = False
        user.categories = self.cleaned_data["categories"]
        user.save()
        text = 'Зарегистрирована новая публичная личность: <a href="%s">%s</a>' % (request.build_absolute_uri(reverse('person-detail', kwargs={'pk':user.id})), user)
        send_mail(
            'Регистрация публичной личности',
            text,
            'no-reply@meetmehub.com',
            settings.RECIPIENTS,
            fail_silently=False,
            html_message=text,
        )
        return user


class SMSForm(forms.Form):
    username = forms.CharField(label=_("Телефон"),
                               min_length=app_settings.USERNAME_MIN_LENGTH,
                               widget=forms.TextInput(
                                   attrs={'placeholder': _('Телефон'),
                                          'autofocus': 'autofocus',
                                          'readonly': 'readonly',
                                          'data-inputmask': "'alias': 'phone'",
                                          }))
    key = forms.CharField(label=_("Код из СМС"),
                               widget=forms.TextInput(
                                   attrs={'placeholder':
                                              _('Код из СМС'),
                                          }))

    def login(self, request, user, redirect_url=None, login_remember=None):
        ret = perform_login(request, user,
                            email_verification=app_settings.EMAIL_VERIFICATION,
                            redirect_url=redirect_url)
        remember = app_settings.SESSION_REMEMBER
        if remember is None:
            remember = login_remember
        if remember:
            request.session.set_expiry(app_settings.SESSION_COOKIE_AGE)
        else:
            request.session.set_expiry(0)
        return ret

    def clean_username(self):
        username = self.cleaned_data["username"]
        if username.find("@") == -1:
            username = username.replace("+", "").replace("(", "").replace(")", "").replace("+", "").replace("-", "").replace(" ", "").strip()
        return username

    def clean(self):
        cleaned_data = super(SMSForm, self).clean()
        try: user = get_user_model().objects.get(username=cleaned_data.get("username"))
        except:
            raise forms.ValidationError(
                    _("Нет пользователя с таким номером телефона")
                )
        try: sms = SMSConfirmation.objects.filter(user=user).order_by("-created")[0]
        except: sms = None

        if not sms or sms.key_expired():
            raise forms.ValidationError(
                _("Срок действия кода истек. Повторите отправку SMS")
            )
        if int(sms.key) != int(self.cleaned_data.get("key")):
            raise forms.ValidationError(
                _("Неверный код из SMS")
            )

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ("photo", "username", "nickname", "first_name", "last_name", "categories", "about", "id")
        help_texts = {
            "username": "",
        }
        labels = {
            "username": _('Телефон'),
        }
        widgets = {
            "username": forms.TextInput(attrs={
                "readonly": "readonly",
            })
        }

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        if not self.instance.is_public:
            del self.fields["categories"]
            del self.fields["about"]


class ApproveForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ("id",)