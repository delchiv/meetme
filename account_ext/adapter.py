import random
import requests

from django.http import HttpResponseRedirect
from django.utils import timezone

from allauth.account import utils, adapter
from allauth.compat import reverse


class DefaultAccountAdapter(adapter.DefaultAccountAdapter):

    def respond_user_inactive(self, request, user, redirect_url=None):
        url = reverse('account_inactive')
        if redirect_url:
            url+="?next="+redirect_url
        return HttpResponseRedirect(url)

    def confirm_sms(self, request, user):
        """
        Marks the email address as confirmed on the db
        """
        user.is_active = True
        user.save()

    def send_confirmation_sms(self, request, smsconfirmation, signup):
        smsconfirmation.key = random.randint(0000, 9999)
        smsconfirmation.sent = timezone.now()
        smsconfirmation.save()
        phone = smsconfirmation.user.username
        href = "https://sms.ru/sms/send?api_id=EEEC990B-7682-EA6A-8EFF-25C141F82D56&to=%s&msg=%04d&json=1&from=Tugush" % (phone, smsconfirmation.key)
        requests.post(href)

    def clean_username(self, username, shallow=False):
        username = username.replace("+", "").replace("(", "").replace(")", "").replace("+", "").replace("-", "").replace(" ", "").strip()
        return super(DefaultAccountAdapter, self).clean_username(username, shallow)
