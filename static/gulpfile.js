
var gulp = require('gulp');
var sass = require('gulp-sass');
var nunjucks = require('gulp-nunjucks');
var imagemin = require('gulp-imagemin');
var svgSprite = require('gulp-svg-sprite');
var svgmin = require('gulp-svgmin');
var cheerio = require('gulp-cheerio');
var	replace = require('gulp-replace');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');


gulp.task('sass', function(){ // Создаем таск "sass"
    return gulp.src('./sass/*.scss') // Берем источник
        .pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
        .pipe(gulp.dest('./css')) // Выгружаем результата в папку app/css
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(browserSync.reload({stream: true})); // Обновляем CSS на странице при изменении
});

gulp.task('img', function() {
    gulp.src([
            './assets/img/*.+(jpg|png|svg)',
            '!./assets/img/icon-*.svg',
        ])
        .pipe(imagemin())
        .pipe(gulp.dest('./img'))
});

gulp.task('svg', function() {
    gulp.src('./assets/img/icons/icon-*.svg')
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill, style and stroke declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').attr('fill', 'none');
                $('[stroke]').attr('stroke', 'currentColor');
                $('[style]').removeAttr('style');
            },
            parserOptions: {xmlMode: true}
        }))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: "../img/sprite.svg",
                    render: {
                        scss: {
                            dest:'../sassgit/_sprite.scss',
                            template: "./sass/templates/_sprite_template.scss"
                        }
                    }
                }
            }
        }))
        .pipe(gulp.dest('.'));
});


gulp.task('default', [], function() {
    gulp.watch('./assets/img/*', ['images']); // Наблюдение за sass файлами в папке sass
    gulp.watch('./sass/*.scss', ['sass']); // Наблюдение за sass файлами в папке sass
});
