from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from modeltranslation.admin import TabbedDjangoJqueryTranslationAdmin

from .models import FlatBlock


class FlatBlockAdmin(TabbedDjangoJqueryTranslationAdmin):

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name.startswith('content_'):
            kwargs['widget'] = CKEditorUploadingWidget(attrs={'class':'mt mt-field-content-%s' % db_field.name.replace('content_', '')})
        return super(FlatBlockAdmin, self).formfield_for_dbfield(db_field, **kwargs)


# We have to unregister the normal admin, and then reregister ours
admin.site.unregister(FlatBlock)
admin.site.register(FlatBlock, FlatBlockAdmin)