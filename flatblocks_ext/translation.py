from modeltranslation.translator import translator, TranslationOptions

from .models import FlatBlock


class FlatBlockTranslationOptions(TranslationOptions):
    fields = ('header', 'content',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(FlatBlockTranslationOptions, self).formfield_for_dbfield(db_field, **kwargs)
        self.patch_translation_field(db_field, field, **kwargs)
        return field


translator.register(FlatBlock, FlatBlockTranslationOptions)