from allauth.socialaccount.providers.vk import provider


def get_profile_url(self):
    PROFILE_URL = 'http://vk.com/id'
    return PROFILE_URL + str(self.account.uid)

provider.VKAccount.get_profile_url = get_profile_url
