from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from modeltranslation.admin import TabbedDjangoJqueryTranslationAdmin

from .models import CKFlatPage, FlatPage


class CKFlatPageAdmin(TabbedDjangoJqueryTranslationAdmin):
    list_display = ('order', 'url', 'title', 'is_visible',)
    list_editable = ('order', 'is_visible',)
    list_display_links = ('url',)
    ordering = ('order',)
    fieldsets = (
        (None, {'fields': (('order', 'url', 'is_visible'), 'title', 'content', 'sites')}),
        (_('SEO'), {
            'classes': ('collapse',),
            'fields': ('description', 'keywords'),
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': ('registration_required', 'template_name'),
        }),
    )

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name.startswith('content_'):
            kwargs['widget'] = CKEditorUploadingWidget(attrs={'class':'mt mt-field-content-%s' % db_field.name.replace('content_', '')})
        return super(CKFlatPageAdmin, self).formfield_for_dbfield(db_field, **kwargs)


# We have to unregister the normal admin, and then reregister ours
admin.site.unregister(FlatPage)
admin.site.register(CKFlatPage, CKFlatPageAdmin)