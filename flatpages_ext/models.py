from django.db import models
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import ugettext_lazy as _


# Create your models here.

class CKFlatPage(FlatPage):
    order = models.PositiveIntegerField(_('№ п/п'), default=0)
    is_visible = models.BooleanField(_('Показывать?'), default=True)
    description = models.CharField(verbose_name=_(u"meta description"), max_length=255, null=True, blank=True)
    keywords = models.CharField(verbose_name=_(u"meta keywords"), max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = _('flat page')
        verbose_name_plural = _('flat pages')