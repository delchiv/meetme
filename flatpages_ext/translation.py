from modeltranslation.translator import translator, TranslationOptions

from .models import FlatPage, CKFlatPage


class FlatPageTranslationOptions(TranslationOptions):
    fields = ('title', 'content',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(FlatPageTranslationOptions, self).formfield_for_dbfield(db_field, **kwargs)
        self.patch_translation_field(db_field, field, **kwargs)
        return field


class CKFlatPageTranslationOptions(TranslationOptions):
    fields = ()

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(CKFlatPageTranslationOptions, self).formfield_for_dbfield(db_field, **kwargs)
        self.patch_translation_field(db_field, field, **kwargs)
        return field


translator.register(FlatPage, FlatPageTranslationOptions)
translator.register(CKFlatPage, CKFlatPageTranslationOptions)