from django import template
from django.contrib.flatpages.templatetags import flatpages


register = template.Library()


class FlatpageNode(flatpages.FlatpageNode):

    def render(self, context):
        super(FlatpageNode, self).render(context)
        context[self.context_name] = context[self.context_name].prefetch_related('ckflatpage').filter(ckflatpage__is_visible=True).order_by('ckflatpage__order')
        return ''

flatpages.FlatpageNode = FlatpageNode

@register.tag
def get_flatpages(parser, token):
    return flatpages.get_flatpages(parser, token)