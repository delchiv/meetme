import parsedatetime as pdt

from django import forms
from django.utils.translation import pgettext, ugettext as _


from .models import Meeting


class MeetingCreateForm(forms.ModelForm):
    mdate = forms.CharField(label=_("Дата и время"), widget=forms.TextInput(attrs={"type": "text"}))

    class Meta:
        model = Meeting
        fields = '__all__'
        exclude = ["state",]

        widgets = {
            "mfrom": forms.HiddenInput(),
            "mto": forms.HiddenInput(),
        }

    def clean(self):
        cleaned_data = super(MeetingCreateForm, self).clean()
        cal = pdt.Calendar()
        date = self.cleaned_data.get('mdate', '').replace("T", " ")
        date, result = cal.parseDT(date)
        cleaned_data["mdate"] = date

        return cleaned_data
