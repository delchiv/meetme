from django.conf.urls import url

from . import views

app_name = 'meetings'
urlpatterns = [
#    url(r'^anketa/$', views.anketa, name='anketa'),
    url(r'create/$', views.MeetingCreateView.as_view(), name='create'),
    url(r'(?P<pk>\d+)/$', views.MeetingDetailView.as_view(), name='detail'),
]
