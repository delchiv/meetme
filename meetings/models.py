from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import pgettext, ugettext as _
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.shortcuts import reverse
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail

# Create your models here.


class MeetingType(models.Model):
    title = models.CharField(_("Название"), max_length=200)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Тип встречи')
        verbose_name_plural = _('Типы встречи')


class MeetingGoal(models.Model):
    title = models.CharField(_("Название"), max_length=200)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Цель встречи')
        verbose_name_plural = _('Цели встречи')

MEETING_STATUSES = [
    (0, _("Новая")),
    (1, _("Согласована")),
    (2, _("Оплачена")),
    (3, _("Проведена")),
    (4, _("Завершена")),
]

class Meeting(models.Model):
    mfrom = models.ForeignKey(get_user_model(), related_name="+", verbose_name=_("Кто"))
    mto = models.ForeignKey(get_user_model(), related_name="+", verbose_name=_("Кому"))
    about = models.TextField(_("Коротко о себе"))
    is_familiar = models.BooleanField(_("Знаете ли вы этого человека"))
    mtype = models.ForeignKey(MeetingType, verbose_name=_("Тип встречи"))
    goal = models.ForeignKey(MeetingGoal, verbose_name=_("Цель встречи"), blank=True, null=True)
    goal_str = models.CharField(_("Другая"), max_length=300, blank=True, null=True)
    mdate = models.DateTimeField(_("Дата и время"))
    is_money = models.BooleanField(_("Готов предложить деньги"))
    money = models.FloatField(_("Сумма"), blank=True, null=True)
    is_other = models.BooleanField(_("Другие ценности"))
    other = models.CharField(_("Описание"), max_length=300, blank=True, null=True)
    state = models.IntegerField(_("Статус"), choices=MEETING_STATUSES, default=0)
    created = models.DateTimeField(_('Создана'), auto_now_add=True)
    changed = models.DateTimeField(_('Изменен статус'), auto_now=True)

    def __str__(self):
        return "%s %s" % (self.mfrom, self.mto)

    class Meta:
        verbose_name = _('Встреча')
        verbose_name_plural = _('Встречи')


@receiver(pre_save, sender=Meeting)
def notify_state_change(sender, **kwargs):
    instance = kwargs.pop("instance")
    if instance.id:
        old_state = instance.__class__.objects.get(id=instance.id).get_state_display()
        if instance.get_state_display() != old_state and instance.mfrom.email:
            current_site = get_current_site(request=None)
            text = 'Статус Вашей заявки изменен с %s на %s. <a href="http://%s%s">Подробнее...</a>' % (old_state, instance.get_state_display(), current_site.domain, reverse('meetings:detail', kwargs={"pk":instance.id}))
            send_mail(
                'Изменение статуса заявки',
                text,
                'no-reply@meetmehub.com',
                [instance.mfrom.email],
                fail_silently=False,
                html_message=text,
            )