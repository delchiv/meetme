from modeltranslation.translator import register, TranslationOptions

from .models import MeetingGoal, MeetingType


@register(MeetingGoal)
class MeetingGoalTranslationOptions(TranslationOptions):
    fields = ('title', )


@register(MeetingType)
class MeetingTypeTranslationOptions(TranslationOptions):
    fields = ('title', )

