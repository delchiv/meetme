# coding: utf-8

from django.template.loader import render_to_string
from django.http import JsonResponse, Http404
from django.conf import settings
from django.core.mail import send_mail
from django.views.generic import CreateView, DetailView
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib import messages
from django.utils.translation import pgettext, ugettext as _
from django.shortcuts import reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from comments.forms import CommentForm
from comments.models import Comment
from .forms import MeetingCreateForm
from .models import Meeting

# Create your views here.

class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse({"html": render_to_string(self.template_name, request=self.request, context={"form": form})})
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
            }
            return JsonResponse(data)
        else:
            return response


template = """
    Заявка на встречу с <a href="%(mto_link)s">%(mto)s</a>

    Меня зовут: <a href="%(mfrom_link)s">%(mfrom)s</a>
    Обо мне: %(about)s

    Знаете ли вы этого человека: %(is_familiar)s
    Тип встречи: %(mtype)s
    Цель встречи: %(goal)s
          другое: %(goal_str)s
    Дата и время: %(mdate)s
    Готов предложить деньги: %(money)s
    Другие ценности: %(other)s
"""

class MeetingCreateView(AjaxableResponseMixin, CreateView):
    model = Meeting
    form_class = MeetingCreateForm
    template_name = "meetings/meeting_form.html"

    def get_success_url(self):
        return "/"

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, _('Заявка успершно создана'))
        mto = form.cleaned_data["mto"]
        mfrom = form.cleaned_data["mfrom"]
        context = form.cleaned_data.copy()
        context.update({
            "mto": str(mto),
            "mto_link": self.request.build_absolute_uri(reverse('person-detail', kwargs={'pk':mto.id})),
            "mfrom": str(mfrom),
            "mfrom_link": self.request.build_absolute_uri(reverse('person-detail', kwargs={'pk':mfrom.id})),
        })
        text = template % context
        send_mail(
            'Заявка',
            text,
            'no-reply@meetmehub.com',
            settings.RECIPIENTS,
            fail_silently=False,
            html_message=text
        )
        return super(MeetingCreateView, self).form_valid(form)

    def dispatch(self, request, *args, **kwargs):
        if request.method == "GET" and not request.is_ajax():
            raise Http404
        return super(MeetingCreateView, self).dispatch(request, *args, **kwargs)


@method_decorator(login_required, name='dispatch')
class MeetingDetailView(UserPassesTestMixin, DetailView):
    model = Meeting

    def test_func(self):
        return self.request.user.is_superuser or self.request.user == self.get_object().mfrom

    def get_context_data(self, **kwargs):
        context = super(MeetingDetailView, self).get_context_data(**kwargs)

        if self.object.state == 3:
            context["form"] = CommentForm(initial={"cfrom":self.object.mfrom, "cto":self.object.mto})
        elif self.object.state == 4:
            comment = Comment.objects.filter(cfrom=self.object.mfrom, cto=self.object.mto)
            if comment:
                context["comment"] = comment[0]

        return context

