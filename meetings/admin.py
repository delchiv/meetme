from django.contrib import admin

# Register your models here.

from .models import MeetingType, MeetingGoal, Meeting


class MeetingAdmin(admin.ModelAdmin):
    list_display = ("mfrom", "mto", "mdate", "state", "changed",)
    list_editable = ("state", )
    list_filter = ("state", )


admin.site.register(MeetingType)
admin.site.register(MeetingGoal)
admin.site.register(Meeting, MeetingAdmin)


