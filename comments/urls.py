from django.conf.urls import url

from . import views

app_name = 'comments'
urlpatterns = [
    url(r'create/$', views.CommentCreateView.as_view(), name='create'),
]
