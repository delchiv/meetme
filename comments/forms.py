from django import forms

from .models import Comment


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = '__all__'

        widgets = {
            "cfrom": forms.HiddenInput(),
            "cto": forms.HiddenInput(),
            "rate": forms.NumberInput(attrs={"class": "star-rating"}),
        }