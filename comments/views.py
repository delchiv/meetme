from django.shortcuts import render
from django.views.generic import CreateView
from django.contrib.auth.mixins import UserPassesTestMixin
from django.http import JsonResponse, Http404
from django.contrib import messages
from django.utils.translation import pgettext, ugettext as _

from meetings.views import AjaxableResponseMixin
from meetings.models import Meeting
from .models import Comment
from .forms import CommentForm

# Create your views here.

class CommentCreateView(AjaxableResponseMixin, UserPassesTestMixin, CreateView):
    model = Comment
    form_class = CommentForm
    template_name = "comments/comment_form.html"

    def get_success_url(self):
        return "/"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user == self.get_object().cfrom

    def dispatch(self, request, *args, **kwargs):
        if request.method == "GET" and not request.is_ajax():
            raise Http404
        return super(CommentCreateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, _('Комментарий успершно добавлен'))
        meeting = Meeting.objects.filter(mfrom=form.cleaned_data["cfrom"], mto=form.cleaned_data["cto"])
        if meeting:
            meeting = meeting[0]
            meeting.state = 4
            meeting.save()

        return super(CommentCreateView, self).form_valid(form)
