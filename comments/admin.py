from django.contrib import admin

from .models import Comment

class CommentAdmin(admin.ModelAdmin):
    list_display = ("cfrom", "cto", "rate", "created",)
    list_editable = ("rate", )


admin.site.register(Comment, CommentAdmin)

