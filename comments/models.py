from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import pgettext, ugettext as _
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.


class Comment(models.Model):
    cfrom = models.ForeignKey(get_user_model(), related_name="+", verbose_name=_("От кого"))
    cto = models.ForeignKey(get_user_model(), related_name="+", verbose_name=_("Кому"))
    rate = models.FloatField(_('Оценка'), default=0)
    created = models.DateTimeField(_('Отправлен'), auto_now_add=True)
    text = models.TextField(_('Текст'), max_length=1000)

    def __str__(self):
        return "%s %s" % (self.cfrom, self.cto)

    class Meta:
        verbose_name = _('Комментарий')
        verbose_name_plural = _('Комментарии')


@receiver(post_save, sender=Comment)
def comment_added(sender, **kwargs):
    instance = kwargs.pop("instance")
    instance.cto.recalc_rate()