import re

from django import template
from django.utils.html import conditional_escape, mark_safe

register = template.Library()


@register.filter(needs_autoescape=True)
def highlight(text, sterm, autoescape=None):
    if autoescape:
        esc = conditional_escape
    else:
        esc = lambda x: x
    pattern = re.compile('(?P<q>%s)' % esc(sterm), re.IGNORECASE)
    print(pattern)
    result = pattern.sub('<strong class="text-warning">\g<q></strong>', '%s' % text)
    print(result)
    return mark_safe(result)