from modeltranslation.translator import register, TranslationOptions

from .models import Banner, Category, Fund


@register(Banner)
class BannerTranslationOptions(TranslationOptions):
    fields = ('title', 'text',)


@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('title', )


@register(Fund)
class FundTranslationOptions(TranslationOptions):
    fields = ('title', )
