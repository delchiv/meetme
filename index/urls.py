from django.conf.urls import url

from . import views

app_name = 'index'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>\d+)/$', views.CategoryDetailView.as_view(), name='category-detail'),
    url(r'^search/$', views.UserSearchView.as_view(), name='person-search'),
]
