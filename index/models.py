# coding: utf-8

import os

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Banner(models.Model):
    num = models.IntegerField(_('N п/п'), null=True, blank=True)
    image = models.ImageField(_('Изображение'), upload_to='banners/%Y/%m/%d')
    title = models.CharField(_('Заголовок'), max_length=250)
    text = models.TextField(_('Текст'), null=True, blank=True)
    enabled = models.BooleanField(_('Показывать?'), default=False)

    class Meta:
        verbose_name = _('Банер')
        verbose_name_plural = _('Банеры')

    def __str__(self):
        return self.title

    def delete(self, *args, **kwargs):
        self.image.delete()
        super(Banner, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if self.pk:
            old = self.__class__._default_manager.get(pk=self.pk)
            if old.image.name and (not self.image._committed or not self.image.name):
                old.image.delete(save=False)
        super(Banner, self).save(*args, **kwargs)


class Category(models.Model):
    image = models.ImageField(_('Изображение'), upload_to='categories/%Y/%m/%d')
    title = models.CharField(_('Заголовок'), max_length=250)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Категория')
        verbose_name_plural = _('Категории')

    def delete(self, *args, **kwargs):
        self.image.delete()
        super(Category, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if self.pk:
            old = self.__class__._default_manager.get(pk=self.pk)
            if old.image.name and (not self.image._committed or not self.image.name):
                old.image.delete(save=False)
        super(Category, self).save(*args, **kwargs)


class Fund(models.Model):
    image = models.ImageField(_('Изображение'), upload_to='categories/%Y/%m/%d')
    title = models.CharField(_('Название'), max_length=250)
    link = models.CharField(_('Ссылка'), max_length=250)
    enabled = models.BooleanField(_('Показывать?'), default=False)
    num = models.IntegerField(_('N п/п'), null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Партнер')
        verbose_name_plural = _('Партнеры')

    def delete(self, *args, **kwargs):
        self.image.delete()
        super(Fund, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if self.pk:
            old = self.__class__._default_manager.get(pk=self.pk)
            if old.image.name and (not self.image._committed or not self.image.name):
                old.image.delete(save=False)
        super(Fund, self).save(*args, **kwargs)