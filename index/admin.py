from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from modeltranslation.admin import TabbedDjangoJqueryTranslationAdmin

from .models import Banner, Category, Fund
from .actions import delete_selected as delete_each_selected

# Register your models here.


class BannerAdminForm(forms.ModelForm):
    class Meta:
        model = Banner
        fields = '__all__'
        #widgets = {
        #    'text_ru': CKEditorUploadingWidget(),
        #    'text_en': CKEditorUploadingWidget(),
        #}


class BannerAdmin(TabbedDjangoJqueryTranslationAdmin):
    form = BannerAdminForm
    actions = ['delete_selected',]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name.startswith('text'):
            kwargs['widget'] = CKEditorUploadingWidget()
        return super(BannerAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    def delete_selected(modeladmin, request, queryset):
        return delete_each_selected(modeladmin, request, queryset)
    delete_selected.short_description = _("Delete selected %(verbose_name_plural)s")


class CategoryAdmin(TabbedDjangoJqueryTranslationAdmin):
    model = Category
    actions = ['delete_selected', ]

    def delete_selected(modeladmin, request, queryset):
        return delete_each_selected(modeladmin, request, queryset)
    delete_selected.short_description = _("Delete selected %(verbose_name_plural)s")


class FundAdmin(TabbedDjangoJqueryTranslationAdmin):
    model = Fund
    actions = ['delete_selected', ]

    def delete_selected(modeladmin, request, queryset):
        return delete_each_selected(modeladmin, request, queryset)
    delete_selected.short_description = _("Delete selected %(verbose_name_plural)s")


admin.site.register(Banner, BannerAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Fund, FundAdmin)