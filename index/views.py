from django.db.models import Count, Q
from django.views.generic import TemplateView, ListView
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model

from .models import Banner, Category, Fund

# Create your views here.

class IndexView(TemplateView):
    template_name = 'index/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['banners'] = Banner.objects.filter(enabled=True).order_by('num')
        context['categories'] = Category.objects.filter(user__is_public=True, user__is_approved=True).select_related().annotate(users_count=Count('user__id'))
        context['funds'] = Fund.objects.all()
        context['popular'] = get_user_model().objects.filter(is_public=True, is_approved=True).order_by('-rate')[:3]
        try: context['ajax_after_done'] = self.request.session.pop("ajax_after_done")
        except: pass
        return context


@method_decorator(login_required, name='dispatch')
class CategoryDetailView(DetailView):
    model = Category


@method_decorator(login_required, name='dispatch')
class UserSearchView(ListView):
    model = get_user_model()
    template_name = 'index/search.html'

    def get_context_data(self, *args, **kwargs):
        context = super(UserSearchView, self).get_context_data(*args, **kwargs)

        context['q'] = self.request.GET.get('q', '')

        return context

    def get_queryset(self):
        try:
            q = self.request.GET['q']
        except:
            q = ''
        if (q != '' and len(q) >= 3):
            q = (Q(first_name__icontains=q) | Q(last_name__icontains=q) | Q(nickname__icontains=q) | Q(about__icontains=q))
            object_list = self.model.objects.filter(is_public=True, is_approved=True).filter(q)
        else:
            object_list = []
        return object_list
